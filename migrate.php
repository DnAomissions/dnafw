<?php
require_once 'init.php';

header('location: '.url('/'));

$command = "mysql ";
if (trim($db_user) != '')
{
    $command = $command." -u {$db_user}";
}
if (trim($db_pass) != '')
{
    $command = $command." -p {$db_pass}";
}
if (trim($db_host) != '')
{
    $command = $command." -h {$db_host}";
}

$command = $command." < ";

echo "Importing Database...";
$output = shell_exec($command . ROOT.'/database/database.sql');
sleep(1);
echo "\n";
echo "Importing Done...";
?>