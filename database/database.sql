  --
  -- Database: `dnafw`
  --

  DROP DATABASE IF EXISTS dnafw;

  CREATE DATABASE dnafw;
  USE dnafw;

  -- --------------------------------------------------------

  --
  -- Table structure for table `users`
  --
  DROP TABLE IF EXISTS `users`;
  CREATE TABLE `users` (
    `user_id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `username` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `role` enum("admin", "user") NOT NULL,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`user_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  --  INSERT Default User 
  --  Username : admin
  --  Password : admin
  --

  INSERT INTO `users` SET `name`='Admin', `username`='admin', `password`='21232f297a57a5a743894a0e4a801fc3', `role`='admin';

  --
  -- Table structure for table `products`
  --
  DROP TABLE IF EXISTS `products`;
  CREATE TABLE `products` (
    `product_id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `description` TEXT,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`product_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;