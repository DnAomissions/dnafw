<form class="card z-depth-3" action="<?=(($product ?? '') != '') ? url('/products/update', $product['product_id']) : url('/products/store')?>" method="post">
    <input type="hidden" name="model" value=products id="model"/>
    <div class="card-content row">
        <div class="col s12 m8 offset-m2">
            <div class="input-field" style="display:none;">
                <input name="product_id" id="product_id" value="<?= $product['product_id'] ?? ''?>" type="text" class="validate" readonly>
                <label for="product_id">Product ID</label>
            </div>
            <div class="input-field">
                <input name="name" id="name" value="<?= $product['name'] ?? ''?>" type="text" class="validate" required autofocus>
                <label for="name">Name</label>
            </div>
            <div class="input-field">
                <textarea id="description" name="description" class="materialize-textarea validate"><?= $product['description'] ?? ''?></textarea>
                <label for="description">Textarea</label>
            </div>
        </div>
    </div>
    <div class="card-action row">
        <div class="col s12 m8 offset-2">
            <button id="button-submit" type="submit" name="<?=(($product ?? '') != '') ? 'update' : 'store'?>" class="btn green"><?=(($product ?? '') != '') ? 'Update' : 'Save'?></button>
        </div>
    </div>
</form>