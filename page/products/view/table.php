<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title">Data Products</span>
            <div class="actions">
                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
            </div>
        </div>
        <table class="highlight datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($products as $key => $row) 
            {
            ?>
                <tr>
                    <td><?=$row['name']?></td>
                    <td><?=$row['description']?></td>
                    <td>
                        <form action="<?=url('/products/destroy', $row['product_id'])?>" class="form-destroy" onsubmit="return confirm('Are you sure to delete <?=$row['name'] ?>?');" method="post">
                            <input type="hidden" name="model" value=products id="model"/>
                            <a href="<?=url('/products/edit', $row['product_id'])?>" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Edit"><i class="material-icons blue-text">edit</i></a>
                            <input name="product_id" id="product_id" value="<?= $row['product_id'] ?? ''?>" type="hidden"/>
                            <button type="submit" name="destroy" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Delete"><i class="material-icons red-text">delete_forever</i></button>
                        </form>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>