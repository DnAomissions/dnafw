<link rel="stylesheet" href="<?=asset('datatable/datatable.css')?>">

<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

<!-- Dropdown Structure -->
    <!-- Dropdown Admin -->
    <ul id="dropdown_admin" class="dropdown-content">
<?php
    if($auth->role == 'admin')
    {
?>
        <li><a href="<?=url('/users')?>">Users</a></li>
<?php
    }
?>
    </ul>
    <!-- Dropdown Master Data -->
    <ul id="dropdown_master_data" class="dropdown-content">
        <li><a href="<?=url('/products')?>">Products</a></li>
    </ul>

    <!-- Dropdown User -->
    <ul id="dropdown_user" class="dropdown-content">
        <li><a href="#" id="btn_logout"><i class="material-icons left">logout</i> Logout</a></li>
    </ul>
<nav>
    <div class="nav-wrapper">
        <ul class="left hide-on-med-and-down">
            <li><a href="<?=url('/')?>"><i class="material-icons left">home</i> Home</a></li>
            <li><a class="dropdown-trigger" href="#!" data-target="dropdown_master_data"><i class="material-icons left">list</i> Master Data <i class="material-icons right">arrow_drop_down</i></a></li>
            <?php
                if($auth->role == 'admin')
                {
            ?>
            <li><a class="dropdown-trigger" href="#!" data-target="dropdown_admin"><i class="material-icons left">person</i> Admin <i class="material-icons right">arrow_drop_down</i></a></li>
            <?php
                }
            ?>
        </ul>
        <ul class="right hide-on-med-and-down">
            <!-- Dropdown Trigger -->
            <li><a class="dropdown-trigger" href="#!" data-target="dropdown_user"><?=$auth->name?><i class="material-icons right">arrow_drop_down</i></a></li>
        </ul>
    </div>
</nav>

<form action="<?=url('/auth/logout')?>" id="form_logout" method="post" style="display:none;">
    <input type="hidden" name="user_id" value="<?=$auth->user_id?>"/>
</form>

<!-- Start Content -->
<br/>
<div style="width:95%; margin:0 auto;">
    <?php
        include load_page($page);
    ?>
</div>


<!-- End Content -->

<script src="<?=asset('datatable/datatable.js')?>"></script>

<script>
    $(document).ready(function(){
        $('#btn_logout').click(function(){
            $('#form_logout').submit()
        })
    })
</script>