<nav class="z-depth-2">
    <div class="nav-wrapper row cyan">
        <div class="col s12">
        <?php
            foreach ($breadcrumb_items as $key => $breadcrumb_item) {
                if($breadcrumb_item['link'] == 'javascript:void(0)' || $breadcrumb_item['link'] == '#')
                {
        ?>
            <span class="breadcrumb"><?=$breadcrumb_item['title']?></span>
        <?php
                }else{

        ?>
            <a href="<?=$breadcrumb_item['link']?>" class="breadcrumb"><?=$breadcrumb_item['title']?></a>
        <?php
                }
            }
        ?>
        </div>
    </div>
</nav>