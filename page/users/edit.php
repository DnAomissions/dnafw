<?php
    // Role Permission
    if($auth->role != 'admin')
    {
        echo "<script>window.location.replace('".url('/')."')</script>";
        exit;
    }

    $user = new User();
    $user = $user->find($_GET['id']);

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Users',
            'link' => url('/users')
        ],
        [
            'title' => $user['name'],
            'link' => 'javascript:void(0)'
        ],
    ];

    include_once load_component('breadcrumb');


?>
<br>
<?php
    include 'view/form.php';
    
?>