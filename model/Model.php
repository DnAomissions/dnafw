<?php
require_once ROOT.'/library/DB.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

abstract class Model
{
    public $name;
    public $db;
    public $table;
    public $primaryKey = 'id';
    protected $columns = [];
    protected $hide = [];
    protected $timestamps = true;
    protected $many2one = [];
    protected $one2many = [];

    function __construct(array $attributes = [])
    {
        $db = new DB();
        $this->db = $db->mysqli;
    }

    /**
     * Import & Export
     */

    public function export()
    {
        $columns = array_values($this->getColumns());
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $alphabet = range('A', 'Z');
        $REC = [];
        foreach($columns as $COL => $c)
        {
            $sheet->setCellValue($alphabet[$COL].'1', $c);
            $REC += [$alphabet[$COL].'1' => $c];
        }

        $data = $this->select();
        $i = 2;
        foreach($data as $row)
        {
            foreach($columns as $COL => $c)
            {
                $sheet->setCellValue($alphabet[$COL].$i, $row[$c]);
            }
            $i++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(public_path('/tmp/'.$this->name.'.xlsx' ?? '/tmp/'.$this->table.'.xlsx'));
        echo "<a href='".public_url('/tmp/'.$this->name.'.xlsx' ?? '/tmp/'.$this->table.'.xlsx')."' id='downloadExport' download/></a>";
        echo "<script>document.getElementById('downloadExport').click()</script>";
        return true;
    }

    public function import($filepath, $ext)
    {
        $inputFileType = ucfirst($ext);
        $inputFileName = $filepath;

        /**  Create a new Reader of the type defined in $inputFileType  **/
        $reader = IOFactory::createReader($inputFileType);
        /**  Advise the Reader that we only want to load cell data  **/
        // $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($inputFileName);

        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();

        // Delete file after get data
        unlink($inputFileName);
        
        $column_name = $rows[0];
        unset($rows[0]);
        
        if(!$this->checkImportFormat($column_name, $this->columns))
        {
            return false;
        }

        $res = [];
        foreach ($rows as $key => $value) {
            $record = [];
            foreach($column_name as $i => $column){
                $record += $this->setKeyValue($column, $value[$i]);
            }
            $res[] = $record;
        }

        $res = $this->createBatch($res);

        return $res;
    }

    public function checkImportFormat($column_name, $columns_check)
    {
        foreach($column_name as $c)
        {
            if(!in_array($c, $columns_check))
            {
                $this->sessionError("Excel Format not valid!");
                return false;
            }
        }

        return true;
    }

    /**
     * Basic Function
     */
    public function select($conditions = null)
    {
        $columns = $this->getColumns();
        $query = 'SELECT '.$this->columnsToString($columns).' FROM '.$this->table;
        
        if(!is_null($conditions))
        {
            $query = $query.' '.$conditions;
        }

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        $res = [];
        while ($row=$result->fetch_assoc())
        {
            foreach($this->many2one as $j)
            {
                $select = $j['class']->select('WHERE '.$j['pkey'].'='.$row[$j['fkey']]);
                if(count($select) > 0)
                {
                    $row[$j['class']->table] = $select[0];
                }
            }
            foreach($this->one2many as $j)
            {
                $row[$j['class']->table] = $j['class']->select('WHERE '.$j['fkey'].'='.$row[$this->primaryKey]);
            }
            $res[] = $row;
        }
        
		return $res;
    }

    public function find($id)
    {
        return $this->select('WHERE '.$this->primaryKey.'='.$id)[0];
    }

    public function truncate()
    {
        $query = 'TRUNCATE TABLE '.$this->table;

        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        return true;
    }

    public function findOrCreate($array)
    {
        $res = $this->select('WHERE '.$this->queryColumn($array, ' AND ', null).' LIMIT 1')[0];

        if(!count($res) > 0)
        {
            $res = $this->create($array);
        }

        return $res;
    }

    public function createOrUpdate($array)
    {
        $select = $this->select('WHERE '.$this->queryColumn($array, ' AND ', null));

        if(count($select) > 0)
        {
            $res = $this->update($select[0][$this->primaryKey], $array);
        }else{
            $res = $this->create($array);
        }

        return $res;
    }

    public function createBatch($array)
    {
        $res = [];
        foreach($array as $row)
        {
            $res = $this->createOrUpdate($row);
        }

        return $res;
    }

    public function create($array)
    {
        $query = 'INSERT INTO '.$this->table.' SET '.$this->queryColumn($array, ', ', false);
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->queryColumn($array, ' AND ', null),' LIMIT 1');

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data;
    }

    public function update($id, $array)
    {
        $query = 'UPDATE '.$this->table.' SET '.$this->queryColumn($array).' WHERE '.$this->primaryKey.'='.$id;
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->primaryKey.'='.$id);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }

    public function delete($id)
    {
        $query = 'DELETE FROM '.$this->table.' WHERE '.$this->primaryKey.'='.$id;
        $this->db->query($query);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        return true;
    }

    function queryColumn($array, $delimiter = ', ', $is_update = true)
    {
        foreach($this->getColumns(false) as $column)
        {
            if(is_null($is_update) && $column == 'created_at')
            {
                continue;
            }
            if(is_null($is_update) && $column == 'updated_at')
            {
                continue;
            }
            if($is_update && $column == 'created_at')
            {
                continue;
            }
            $value = null;
            if(array_key_exists($column, $array))
            {
                $value = $array[$column];
            }
            $res[] = $this->setColumn($column, $value);
        }
        $res = array_filter($res);

        return implode($delimiter, $res);
    }

    function setColumn($key, $value = null)
    {
        if($key == 'created_at' || $key == 'updated_at')
        {
            $value = date('Y-m-d H:i:s');
        }
        if($value==null){
            return '';
        }
        if(!is_numeric($value))
        {
            return $key.'="'.$value.'"';
        }
        return $key.'='.$value;
    }

    function getColumns($primaryKey = true)
    {
        $columns = $this->columns;
        if($primaryKey)
        {
            array_unshift($columns, $this->primaryKey);
        }
        foreach($this->hide as $hide)
        {
            if (($key = array_search($hide, $columns)) !== false) {
                unset($columns[$key]);
            }
        }
        if($this->timestamps)
        {
            $columns[] = 'created_at';
            $columns[] = 'updated_at';
        }
        return $columns;
    }

    function columnsToString($columns)
    {
        return implode(', ', $columns);
    }

    function sessionError($message)
    {
        $_SESSION['error'] = $message;
    }

    public function setKeyValue($key, $value)
    {
        return [$key => $value];
    }
}
?>