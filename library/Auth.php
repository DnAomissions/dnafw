<?php
require_once 'Session.php';

class Auth extends Session
{
    public $user_id;
    public $username;
    public $name;
    public $role;

    public function isLogedIn()
    {
        $this->checkUser();
        if(isset($this->user_id)
            && isset($this->username)
            && isset($this->name)
            && isset($this->role))
        {
            return true;
        }else{
            return false;
        }
    }

    function checkUser()
    {
        if($this->issetSession('user_id')
            && $this->issetSession('name')
            && $this->issetSession('username')
            && $this->issetSession('role'))
        {
            $this->user_id = $this->getSession('user_id');
            $this->name = $this->getSession('name');
            $this->username = $this->getSession('username');
            $this->role = $this->getSession('role');
        }
    }

    public function destroyUser($user_id)
    {
        if($user_id != $this->user_id)
        {
            $this->setSession('error', 'Cannot Logout with diferent User!');
            return false;
        }
        $this->unsetSession('user_id');
        $this->unsetSession('name');
        $this->unsetSession('username');
        $this->unsetSession('role');

        $this->user_id = null;
        $this->name = null;
        $this->username = null;
        $this->role = null;
    }

    public function setUser($user_id, $name, $username, $role)
    {
        $this->user_id = $user_id;
        $this->name = $name;
        $this->username = $username;
        $this->role = $role;

        $this->setSession('user_id', $this->user_id);
        $this->setSession('name', $this->name);
        $this->setSession('username', $this->username);
        $this->setSession('role', $this->role);
    }
}
?>