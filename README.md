## Installation

1. Copy **_config.php.example_** to **_config.php_** & Set all variable
2. **_cd your_project_path_**
3. **_composer install_**

## File to setup

1. **_config.php_**
2. **_database/database.sql_**
